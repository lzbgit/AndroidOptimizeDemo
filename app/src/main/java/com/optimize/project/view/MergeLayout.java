package com.optimize.project.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.optimize.project.R;

/**
 * 自定义的View，竖直方向的LinearLayout
 */
public class MergeLayout extends LinearLayout {
    public MergeLayout(Context context) {
        super(context);
        // 设置为数值方向的布局
        setOrientation(VERTICAL);
        LayoutInflater.from(context).inflate(R.layout.merge_llayout, this, true);
    }
}