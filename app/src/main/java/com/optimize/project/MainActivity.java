package com.optimize.project;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.ArrayMap;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewStub;
import android.widget.TextView;

import com.optimize.project.JobSchedule.JobScheduleActivity;
import com.optimize.project.JobSchedule.MyJobService;
import com.optimize.project.executor.ExecutorActivity;
import com.optimize.project.netCache.gcm.GCMTestActivity;

@TargetApi(Build.VERSION_CODES.KITKAT)
public class MainActivity extends Activity {

    private ViewStub viewStub;
    private TextView textView;

    private ArrayMap<String,String> arrayMap = new ArrayMap<>();
    private SparseIntArray sparseIntArray = new SparseIntArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewStub = (ViewStub) findViewById(R.id.vs);
        /**空指针 由于viewStub 是延迟加载 必须调用 inflate 才会加载  */
        //textView  = (TextView) findViewById(R.id.hello_tv);
        viewStub.inflate();
        /** 空指针 由于viewStub加载后，将会把其子控件交给其父控件 自己将会隐退 所以此时viewStub 为null  */
        //textView  = (TextView) viewStub.findViewById(R.id.hello_tv);
        /** 正确 查找 */
        textView  = (TextView) findViewById(R.id.hello_tv);
        textView.setText("查找了");
    }


    public void onClick(View view){
        switch (view.getId()){
            case R.id.btn_thread:
                startActivity(new Intent(this, ExecutorActivity.class));
                break;
            case R.id.btn_jobSchedule:
                startActivity(new Intent(this, JobScheduleActivity.class));
                break;
            case R.id.btn_GCm:
                startActivity(new Intent(this, GCMTestActivity.class));
                break;
        }
    }





}
