package com.optimize.project.netCache.gcm;

import android.widget.Toast;

import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;

/**
 *
 * <h3>Description</h3>
 * TODO
 * <h3>Author</h3> luzhenbang
 * <h3>Date</h3> 2018/4/17 16:37
 * <h3>Copyright</h3> Copyright (c)2018 Shenzhen TL  Co., Ltd. Inc. All rights reserved.
 */
public class MyUploadService extends GcmTaskService {

    @Override
    public int onRunTask(TaskParams taskParams) {
        switch (taskParams.getTag()){
            case "test-one":
                Toast.makeText(this, "我是一次性任务", Toast.LENGTH_SHORT).show();
                break;
            case "test-PeriodicTask":
                Toast.makeText(this, "我是周期任务", Toast.LENGTH_SHORT).show();
                break;
        }
        return 0;
    }

    /**
     *
     * 当Google Play Services 或者app更新了，
     * 所有的执行任务都会被移除，为了避免失去你当前的执行任务，
     *  GcmNetworkManager将会调用的onInitializeTasks()方法，
     * 这个方法可以重新调度所有的任务，这个通常用来执行定时任务：
     */
    @Override
    public void onInitializeTasks() {
        super.onInitializeTasks();
    }

    
}
