package com.optimize.project.netCache;

import android.app.Activity;
import android.net.http.HttpResponseCache;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.optimize.project.R;

import java.io.File;
import java.io.IOException;

/**
 *
 *
 * 网络缓存
 *
 *
 */
public class NetCacheActivity extends Activity {

    public static  File CACHE_FILE = null;
    /** 缓存文件大小 10MB*/
    public static final int CACHE_SIZE = 10*1024*1024;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_net_cache);

        CACHE_FILE  = new File(getCacheDir(),"http_cache");
        try {
            HttpResponseCache.install(CACHE_FILE,CACHE_SIZE);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    private void clear(){
        HttpResponseCache httpResponseCache = HttpResponseCache.getInstalled();
        if (null != httpResponseCache){
            try {
                httpResponseCache.delete();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



}
