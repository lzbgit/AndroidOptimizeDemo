package com.optimize.project.netCache.gcm;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.OneoffTask;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.Task;
import com.optimize.project.R;


/**
 *
 * GCM网络管理测试
 * 需要 添加依赖包
 * implementation 'com.google.android.gms:play-services-gcm:10.2.1'
 *<h3>Description</h3>
 * TODO
 *<h3>Author</h3> luzhenbang
 *<h3>Date</h3> 2018/4/17 16:49
 *<h3>Copyright</h3> Copyright (c)${YEAR} Shenzhen TL  Co., Ltd. Inc. All rights reserved.
 */
public class GCMTestActivity extends Activity {

    private GcmNetworkManager mGcmNetworkManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gcmtest);
        /*
         * GcmNetworkManager的引用对象，
         * 这个GcmNetworkManager对象将被用来执行任务
         */
        mGcmNetworkManager = GcmNetworkManager.getInstance(this);

        /*
         * 我们需要Google Play Services 才能在调度方法中用GCM网络管理器来处理任务，
         * 为了安全的使用Google Play Services,我们需要检查它是否存在。
         */
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode == ConnectionResult.SUCCESS) {
            mGcmNetworkManager.schedule(oneTask());
            mGcmNetworkManager.schedule(period());
        } else {
            // Deal with this networking task some other way
            Toast.makeText(this, "Google Play Services 不存在", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 创建当个单个任务
     */
    private Task oneTask(){
        OneoffTask.Builder builder = new OneoffTask.Builder();
        //设置服务
        builder.setService(MyUploadService.class);
        //任务执行的时间段，第一个是下边界，第二个是上边界，单位都是s,这个是强制的
        // Schedule a task to occur between five and fifteen  seconds from now:
        builder.setExecutionWindow(1 ,3 );
        //Tag 用来标记onRunTask 方法中哪个任务是当前执行的,每个tag都应该是唯一的，最大的长度是100.
        builder.setTag("test-one");
        //Update Current 这个决定是否应该替换前面一个拥有同一个tag的任务，
        // 默认是false,所以新的任务不会覆盖已经存在的任务
        builder.setUpdateCurrent(false);
        //设置一个指定的网络状态支运行，如果网络状态是不可用，那么一直等到网络可用再执
        builder.setRequiredNetwork(Task.NETWORK_STATE_CONNECTED);
        //是否应该在设备连接电源的情况下执行任务
        builder.setRequiresCharging(false);
        return builder.build();
    }


    /**
     * 定时任务
     * @return
     */
    private Task period(){
        PeriodicTask.Builder builder = new PeriodicTask.Builder();
        //设置服务
        builder.setService(MyUploadService.class);
        //指定任务最多每间隔多长时间执行一次，单位是s,默认地，
        // 我们不能控制在这个时间段具体哪个时间执行，这个设置是强制要设置的
        builder.setPeriod(10);
        //指定在靠近结束时间（上个参数设置）内执行任务，如果时间段是30s，flex是10,调度器会在20-30之间执行。
        builder.setFlex(5);
        //决定任务是不是应该在重启之后继续存在，默认是true,这个对一次性任务不支持,要求获得”Receive Boot Completed”权限，否则这个设置会被忽略。
        builder.setPersisted(true);
        builder.setTag("test-PeriodicTask");
        return builder.build();
    }


    @Override
    protected void onDestroy() {
        //取消特定tag的任务
        mGcmNetworkManager.cancelTask("test-one",MyUploadService.class);
        //取消所有的任务
//        mGcmNetworkManager.cancelAllTasks(MyUploadService.class);

        super.onDestroy();
    }
}
