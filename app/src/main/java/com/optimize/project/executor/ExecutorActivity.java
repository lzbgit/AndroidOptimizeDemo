package com.optimize.project.executor;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.optimize.project.R;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 线程优化测试
 */
public class ExecutorActivity extends AppCompatActivity {
    /** 获取只有一个worker线程的线程池 多余的线程将会被放置线程队列中  保证所有任务按照指定顺序(FIFO, LIFO, 优先级)执行*/
    private ExecutorService  singleThreadExecutor  = Executors.newSingleThreadExecutor();
    /** 获取可缓存的线程池 线程池无限大 如果线程池的长度超过处理需要 内存将会灵活地回收空闲线程 否则的话会创建新的线程 */
    private ExecutorService  cachedThreadPool  = Executors.newCachedThreadPool();
    /** 获取定长线程池 可控制线程池最大并发数 ，超出的线程将会被放置到请求队列中等待 */
    private ExecutorService fixedThreadPool   = Executors.newFixedThreadPool(5);
    /** 获取定长线程池 可控制线程池最大并发数 ，超出的线程将会被放置到请求队列中等待  该线程池可以定时/周期性执行*/
    private ScheduledExecutorService scheduledExecutorService  = Executors.newScheduledThreadPool(5);

    /**接受 返回值*/
    private ArrayList<Future<String>> futures  = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_executor);



        /**
         *  1 ExecutorService 的submit（） 与execute（）区别
         *  (1) execute（）接受runnable 无返回值
         *  (2) submit接受runnable 、 Callable 都有返回值  但我们实现runnable返回的是void 所以submit接受runnable无意义的
         *  2 Runnable（）与Callable（）区别
         *  （1）Runnable 无返回值
         *  （2）Callable 有返回值
         */

        singleThreadExecutor.execute(new MyTask("singleThreadExecutor"));
        Future<String> future  = singleThreadExecutor.submit(new CallTask());
        futures.add(future);

//        Future<String> future2  = (Future<String>) singleThreadExecutor.submit(new MyTask("singleThreadExecutor"));
//        futures.add(future2);

        /** 延迟3秒执行 */
        scheduledExecutorService.schedule(new MyTask("延迟3秒执行"),3, TimeUnit.SECONDS);
        /** 表示延迟1秒后每5秒执行一次 */
        scheduledExecutorService.scheduleWithFixedDelay(new MyTask("表示延迟1秒后每5秒执行一次"),1,5, TimeUnit.SECONDS);

        for (Future<String> f : futures){
            try {
                String vaule = f.get();
                Log.e(ExecutorActivity.class.getName(),vaule);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }


    }




    @Override
    protected void onDestroy() {
        super.onDestroy();
        scheduledExecutorService.shutdown();
        cachedThreadPool.shutdown();
        fixedThreadPool.shutdown();
        singleThreadExecutor.shutdown();
    }
}
