package com.optimize.project.JobSchedule;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.optimize.project.MainActivity;
import com.optimize.project.R;

import java.util.ArrayList;
import java.util.List;

public class JobScheduleActivity extends AppCompatActivity {

    private int mJobId = 0;
    private JobScheduler scheduler;
    private List<JobInfo> jobInfoArrayList = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_schedule);
        scheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
    }


    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_start:
                //判断sdkAPI 大于 21
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ComponentName componentName = new ComponentName(JobScheduleActivity.this, MyJobService.class);
                    //创建JoB
                    JobInfo.Builder builder = new JobInfo.Builder( ++ mJobId, componentName);
                    /**
                     * 五个约束条件如下：
                     * 1）最小延时

                       2）最晚执行时间

                       3）需要充电

                       4）需要设备为idle（空闲）状态（一般很难达到这个条件吧）

                       5）联网状态（NETWORK_TYPE_NONE--不需要网络，NETWORK_TYPE_ANY--任何可用网络，
                          NETWORK_TYPE_UNMETERED--不按用量计费的网络）
                     */
                    //设置JobService执行的最小延时时间
                    builder.setMinimumLatency(1000);
                    //设置JobService执行的最晚时间
                    builder.setOverrideDeadline(10000);
                    //设置执行的网络条件 NETWORK_TYPE_NONE--不需要网络，NETWORK_TYPE_ANY--任何可用网络，NETWORK_TYPE_UNMETERED--不按用量计费的网络
                    builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED);
//                    builder.setRequiresDeviceIdle(true);//是否要求设备为idle状态
                    builder.setRequiresCharging(true);//是否要设备为充电状态

                    //设置重试机制的时间策略，有线性和指数增长两种方式 这个方法与jobFinished(params, true)有关
                    //BACKOFF_POLICY_EXPONENTIAL 指数  BACKOFF_POLICY_LINEAR 线性
//                    builder.setBackoffCriteria(1000,JobInfo.BACKOFF_POLICY_LINEAR);
                    //设置额外的信息，可以在jobservice中获得到
                    PersistableBundle  bundle = new PersistableBundle();
                    bundle.putString("bundle_key","Hi，Eric ");
                    builder.setExtras(bundle);

                    //这个就是周期性执行 貌似没有用
//                    builder.setPeriodic(1000);
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                        builder.setPeriodic(1000, 60 * 1000L);
//                    } else {
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                            builder.setPeriodic(1000);
//                        }
//                    }
                    //手机重启后，还是会执行jobinfor
//                    builder.setPersisted(false);

                    scheduler.schedule(builder.build());
                }

                break;
            case R.id.btn_cancel://取消
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    scheduler.cancel(mJobId);
                    //取消所有
//                    scheduler.cancelAll();
                }
            case R.id.btn_all://获取所有JOBs
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    jobInfoArrayList = scheduler.getAllPendingJobs();
                    Toast.makeText(this, "jobInfoArrayList:" + jobInfoArrayList, Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}
