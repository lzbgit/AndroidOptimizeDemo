package com.optimize.project.JobSchedule;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;
import android.os.PersistableBundle;
import android.widget.Toast;

/**
 *  Google在Android 5.0中引入JobScheduler来执行一些需要满足特定条件但不紧急的后台任务，
 *  APP利用JobScheduler来执行这些特殊的后台任务时来减少电量的消耗。
 * <h3>Description</h3>
 * TODO
 * <h3>Author</h3> luzhenbang
 * <h3>Date</h3> 2018/4/12 17:46
 * <h3>Copyright</h3> Copyright (c)2018 Shenzhen TL  Co., Ltd. Inc. All rights reserved.
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class MyJobService extends JobService {
    PersistableBundle  bundle = null;

    /**
     * 这个方法是主线程执行的，所以我们耗时操作需要在子线程里面执行。
     * @param params
     * @return 返回false的话，jobfinish()中的true属性不在有用，也就是不再重试。
     * 只有返回true,jobfinish(true)才会有用。如果没有调用jobfinish  Service会直接destory掉
     */
    @Override
    public boolean onStartJob(JobParameters params) {
        bundle  = params.getExtras();
        String value = bundle.getString("bundle_key");
        Toast.makeText(this, value + " 开始工作", Toast.LENGTH_SHORT).show();
//        jobFinished(params,false);
        return false;
    }

    /**
     *
     * @param params
     * @return
     */
    @Override
    public boolean onStopJob(JobParameters params) {
        Toast.makeText(this, "停止工作", Toast.LENGTH_SHORT).show();
        return false;
    }


}
